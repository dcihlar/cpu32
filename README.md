# CPU32

![PCB image](hw/render.jpg)

A simple SBC featuring CPU32 and running Enhanced BASIC.

BASIC programs can be stored into built in flash memory.

There are also some basic peripherals like 4 character LED display,
5 LEDs, 5 switches and 2 buttons.

BASIC CLI is available through the RS-232 interface on 9600 8N1.
If CTS is inactive on startup then a program stored at location 0 will be run automatically.

Be careful to type in commands using the capital letters.

## Non-standard BASIC functions

There are a few non-standard BASIC functions simplifying access to peripherals.

 * `DISPLAY <val>` - displays `<val>` on the 4 character display
 * `BUTTONS` - returns bitmask with the active buttons and switches
 * `LEDS <val>` - sets LEDs to the binary value `<val>`
 * `FLASH` - for firmware upgrade, explained later

## BASIC program storage

`LOAD <n>`, `SAVE <n>` and `EXEC <n>` use flash for program storage.

`<n>` is a program ID ranging from 0 to the end of flash memory.

Programs are stored after the first 16kB of flash
(which contain the BASIC interpreter).

Each program can be up to 8kB in size (2 flash pages).

### Uploading a file

A file from a PC can be uploaded to CPU32,
but there must be a delay of 50ms or more after each line.

It is a good idea to first clear the state using the `NEW` command.

## Firmware

### Compiling

[vasm](http://sun.hasenbraten.de/vasm/index.php?view=relsrc) is used to compile the interpreter.
For Linux it can be compiled with `make CPU=m68k SYNTAX=mot`.

With `vasmm68k_mot` available in `PATH` simply run `make`.

`ehbasic.hex` and `firmware.bin` will be produced as a result.

### Running from RAM

Running `make run` will load `ehbasic.hex` into RAM and run it.
This way developing new features is much faster.

[BDMDongle](https://bitbucket.org/dcihlar/bdmdongle/src/master/) is used for accessing the CPU and its memory.

### Flashing

There are two ways.

One is to `make run` (as described above), and then execute the `FLASH` command,
and copy `firmware.bin` to the serial port
(i.e. `dd if=firmware.bin of=/dev/ttyUSB0`).
Please be patient as the file is first copied into RAM,
its integrity is verified and
then finally burned into flash (which is itself a slow process).

The other way is to run `make burn`.
It will directly burn `firmware.hex` into the flash memory.

### Boot procedure

CPU is initialized and the interpreter is loaded into RAM and executed.

Only during boot the code is executed from the slow flash.
