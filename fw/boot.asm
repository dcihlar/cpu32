	INCLUDE	"cpu32.inc"

	ORG	0

vector
	dc.l		$200000			* init RAM @ end of RAM
	dc.l		code_start			* reset vector

code_start
	MOVE.w	#$2700,sr			* initialize SR

	MOVE.b	#$0C,(SYPCR).l		* disable watchdog, enable monitoring
	MOVE.w	#$7F00,(SYNCR).l		* double clock speed

	MOVE.w	#$0006,(CSBARBT).l	* size(flash)=512k
	MOVE.w	#$00FE,(CSPAR0).l		* CSBOOT=8bit, CS0-2=16bit, CS3-5=discrete output
	MOVE.w	#$0155,(CSPAR1).l		* CS6-10=high address bus

	MOVE.w	#$1007,(CSBAR0).l		* ram @ 0x100000
	MOVE.w	#$1007,(CSBAR1).l

	MOVE.w	#$3c30,(CSOR0).l		* low cs
	MOVE.w	#$5c30,(CSOR1).l		* high cs

	LEA		boot_end,a0			* copy source address
	LEA		$100000,a1			* copy destination address
	MOVE.w	#($4000/4-1),d0		* copy length
copy
	MOVE.l	(a0)+,(a1)+			* copy and increment pointers
	DBF		d0,copy			* loop until done

	BSR		$100000			* run the application
fail
	BRA		fail

boot_end
